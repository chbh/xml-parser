﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml.Linq;

namespace xml_xdoc_parser
{
    internal class Program
    {
        [STAThread]
        private static void Main(string[] args)
        {
            string initialDirectory = @"F:\Projekter\Web Booking\Eksempler\";
            OpenFileDialog open = new OpenFileDialog();
            open.Multiselect = true;
            open.InitialDirectory = initialDirectory;
            open.Title = "Select xml to parse";
            open.Filter = "Xml (.xml)|*.xml";
            var elementNames = new List<string>();

            if (open.ShowDialog() == DialogResult.OK)
            {
                foreach (var filePath in open.FileNames)
                {
                    var xml = XDocument.Load(filePath);

                    foreach (var element in xml.Descendants())
                    {
                        if (!elementNames.Contains(element.Name.LocalName))
                        {
                            elementNames.Add(element.Name.LocalName);
                        }
                    }
                }
            }
            MessageBox.Show("Saving the file.");
            SaveFileDialog save = new SaveFileDialog();
            save.FileName = "elements.txt";
            save.Filter = "Text file (*.txt)|*.txt";
            save.InitialDirectory = initialDirectory;

            if (save.ShowDialog() == DialogResult.OK)
            {
                var file = new StreamWriter(save.FileName, true);
                foreach (var element in elementNames)
                {
                    file.WriteLine(element);
                }
                file.Close();
                MessageBox.Show("Done saving.");
            }
        }
    }
}